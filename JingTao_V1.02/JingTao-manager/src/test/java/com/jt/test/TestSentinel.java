package com.jt.test;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Jarvan
 * 2020/8/15 16:12
 */
/* 测试哨兵机制 */
public class TestSentinel {

    /**
     * 参数说明:
     *    masterName: 主机名称
     *      sentinel: 哨兵节点信息
     */
    @Test
    public void test01(){
        Set<String> sentinels = new HashSet<>();
        String node = "192.168.126.129:26379";
        sentinels.add(node);
        JedisSentinelPool sentinelPool = new JedisSentinelPool("mymaster",sentinels);
        Jedis jedis = sentinelPool.getResource();//获取资源
        jedis.set("sentinel","redis哨兵机制配置成功!");
        System.out.println(jedis.get("sentinel"));
    }
}
