package com.jt.test;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jarvan
 * 2020/8/15 10:22
 */
/* 该类测试Redis分片机制  */
public class TestShards {
    /**
     * 说明:在Linux中有3台Redis,需要通过程序进行动态连接实现数据的存储
     * 思考:数据保存到了哪台redis?
     */
    @Test
    public void test01(){
        List<JedisShardInfo> shardInfos = new ArrayList<>();
        shardInfos.add(new JedisShardInfo("192.168.126.129",6379));
        shardInfos.add(new JedisShardInfo("192.168.126.129",6380));
        shardInfos.add(new JedisShardInfo("192.168.126.129",6381));
        //分片的API
        ShardedJedis shardedJedis = new ShardedJedis(shardInfos);
        shardedJedis.set("2007","亚索,尼玛死了");
        System.out.println(shardedJedis.get("2005"));
    }
}
