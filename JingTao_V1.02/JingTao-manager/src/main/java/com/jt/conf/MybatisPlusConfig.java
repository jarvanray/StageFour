package com.jt.conf;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Jarvan
 * 2020/7/31 15:46
 */
@Configuration  //标识配置类
public class MybatisPlusConfig {
    //将分页的拦截器交给spring管理
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor();
    }
}
