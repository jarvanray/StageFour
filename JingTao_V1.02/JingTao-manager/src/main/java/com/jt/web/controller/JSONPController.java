package com.jt.web.controller;


import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.ItemDesc;
import com.jt.util.ObjectMapperUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jarvan
 * 2020/8/17 17:38
 */
@RestController
public class JSONPController {
    @RequestMapping("/web/testJSONP")
    public JSONPObject jsonp(String callback){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(101L).setItemDesc("我是商品详细信息");
        return new JSONPObject(callback,itemDesc);
    }






    /*
    public String jsonP(String callback){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(101L).setItemDesc("我是商品详细信息");
        String json = ObjectMapperUtil.toJSON(itemDesc);
//        return callback+"({'id':'100','name':'tomcat'})";
        return callback+"("+json+")";
    }
    */
}
