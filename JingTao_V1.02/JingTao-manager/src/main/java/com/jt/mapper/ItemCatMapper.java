package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.ItemCat;

/**
 * @author Jarvan
 * 2020/7/31 16:40
 */
public interface ItemCatMapper extends BaseMapper<ItemCat> {
}
