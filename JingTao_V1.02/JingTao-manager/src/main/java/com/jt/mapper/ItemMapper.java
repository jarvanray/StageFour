package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Item;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface ItemMapper extends BaseMapper<Item>{
    //利用注解执行sql
    @Select("SELECT * FROM tb_item ORDER BY updated DESC LIMIT #{startIndex},#{rows}")
    List<Item> findItemAsPage(int startIndex, Integer rows);

    void deleteItems(Long[] ids);

    void updateStatus(@Param("ids") Long[] ids, @Param("status") Integer status);
}
