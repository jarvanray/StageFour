package com.jt.controller;

import com.jt.pojo.EasyUITable;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;

import com.jt.service.ItemService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/item/")
@RestController  //返回值数据类型都是JSON类型
public class ItemController {
	
	@Autowired
	private ItemService itemService;

	/**
	 * 利用restFul方式实现状态修改
	 * 1./item/reshelf  status=1
	 * 2./item/instock  status=2
	 */
	@RequestMapping("/{status}")
	public SysResult updateStatus(@PathVariable Integer status,Long[] ids){
		itemService.updateStatus(ids,status);
		return SysResult.success();
	}

	/**
	 * 完成商品删除操作
	 * 1.url:item/delete
	 * 2.params:ids=id1,id2,...
	 * 3.return:SystemResult
	 * springMVC知识点:可以根据指定的类型动态的实现参数类型的转化
	 * 				  如果字符串使用","分割,则使用数组的方式接参
	 */
	@RequestMapping("delete")
	public SysResult deleteItems(Long[] ids){
		itemService.deleteItems(ids);
		return SysResult.success();
	}

	/**
	 * 完成商品信息的修改
	 * 1.url:http://localhost:8080/item/update
	 * 2.参数:整个商品表单
	 * 3.返回值结果:SysResult
	 */
	@RequestMapping("update")
	public SysResult updateItem(Item item,ItemDesc itemDesc){
		itemService.updateItem(item,itemDesc);
		return SysResult.success();
	}

	/**
	 * 1.URL地址:http://localhost:8080/item/save
	 * 2.请求参数:整个form表单
	 * 3.返回值结果:SysResult
	 *
	 * 复习:页面中的参数是如何通过springMVC为属性赋值???
	 * 分析1:页面参数提交,一般用前2种方式:1.form表单提交  2.ajax页面提交  3.a标签 参数提交
	 * 		页面参数提交,一般都会遵守协议规范 key=value
	 * 分析2:spring的底层实现servlet,包含了2大请求对象:request对象/response对象
	 * 		servlet如何获取数据?
	 * 			规则:参数提交的名称与MVC中接受的参数的名称必须一致!!!
	 */
	@RequestMapping("save")
	public SysResult saveItem(Item item, ItemDesc itemDesc){
		itemService.saveItem(item,itemDesc);
		return SysResult.success();
		//1.利用对象的get方法,获取对象的属性信息
		//item.getId()--->get去除---获取id的属性(大小写忽略)
		//之后将获取到的值利用对象的set方法为属性赋值
		//request.getParameter("id")
		/*try {
			itemService.saveItem(item);
			return SysResult.success();
		}catch (Exception e){
			e.printStackTrace();
			return SysResult.fail();
		}*/
	}

	/**
	 * 业务:分页展现商品列表,要求将最新最热门的商品首先展现给用户
	 * url: url:'/item/query'
	 * 参数: page=1&rows=20
	 * 返回值: EasyUITable
	 */
	@RequestMapping("query")
	public EasyUITable findItemAsPage(Integer page,Integer rows){
		//1.调用业务层,获取商品分页信息
		return itemService.findItemAsPage(page,rows);
	}

	@RequestMapping("query/item/desc/{itemId}")
	public SysResult findItemDescById(@PathVariable Long itemId){
		ItemDesc itemDesc=itemService.findItemDescById(itemId);
		return SysResult.success(itemDesc);
	}
	
}
