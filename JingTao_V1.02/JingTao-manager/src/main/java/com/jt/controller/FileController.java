package com.jt.controller;

import com.jt.service.FileService;
import com.jt.vo.ImageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author Jarvan
 * 2020/8/5 14:22
 */
@RestController
@RequestMapping("/")
public class FileController {
    @Autowired
    private FileService fileService;

    @RequestMapping("file")
    public String file(MultipartFile fileImage){
        String fileDirPath = "D:/JingTao/images";

        File dirFile = new File(fileDirPath);
        if (!dirFile.exists()){
            dirFile.mkdirs();
        }
        //3.准备文件全路径
        String fileName = fileImage.getOriginalFilename();
        File realFile = new File(fileDirPath+"/"+fileName);
        try {
            fileImage.transferTo(realFile);
            return "文件上传成功!";
        } catch (IOException e) {
            e.printStackTrace();
            return "文件上传失败!";
        }
    }

    @RequestMapping("pic/upload")
    public ImageVo uploadFile(MultipartFile uploadFile){
        return fileService.upload(uploadFile);
    }

}
