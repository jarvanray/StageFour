package com.jt.controller;

import com.jt.anno.CacheFind;
import com.jt.pojo.EasyUITree;
import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Jarvan
 * 2020/7/31 16:34
 */
@RestController
@RequestMapping("/item/cat/")
public class ItemCatController {
    @Autowired
    private ItemCatService itemCatService;

    /**
     * 业务:查询商品分类名称 根据id
     * url地址: http://localhost:8091/item/cat/queryItemName?itemCatId=3
     * 参数:    itemCatId=3
     * 返回值:   商品分类名称
     */
    @RequestMapping("queryItemName")
    @CacheFind(key = "ITEM_CAT_NAME")
    public String findItemCatNameById(Long itemCatId) {

        ItemCat itemCat = itemCatService.findItemCatById(itemCatId);
        return itemCat.getName();
    }

    /**
     * 业务需求: 查询一级商品分类信息
     * sql: select * from tb_item_cat where parent_id=0;
     * url: /item/cat/list
     * 返回值: List<EasyUITree>
     * 实现异步加载: id:xxx
     *  说明:当展开一个封闭的节点,才会发起id的参数请求,前提条件是树必须先初始化
     *      应该先展现一级商品分类信息
     *  判断依据:
     *      id是否为null,如果是则表示第一次查询,需要初始化id
     */
    /**
     * 需求:利用redis缓存实现业务功能
     *  根据Id查询自己目录信息
     *  步骤:
     *      1.key       key="ITEM_CAT_LIST::0"
     *      2.value     List<EasyUITree>~~~转化为Json串
     *      3.第一次:先查询redis缓存,有:走缓存的实现;没有:查询数据库
     * @param id
     * @return
     */
    @RequestMapping("list")
    public List<EasyUITree> findItemCatList(Long id){
        Long parentId = (id==null?0L:id);  //根据parentId=0 查询一级商品分类信息
//		//Long  parentId = 0L;
		return itemCatService.findItemCatListByParentId(parentId);  //版本号 1.0.2 调用次方法 开发人员为xxxx
//		return itemCatService.findItemCatCache(parentId);       //手写缓存

    }



}
