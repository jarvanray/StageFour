package com.jt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	/**
	 * 使用restFul风格
	 * 语法:1)参数与参数之间使用"/"分隔
	 * 		2)参数使用"{}"形式包裹
	 * 		3)定义参数使用特定的注解接收:@PathVariable
	 *
	 * 用法一:
	 * 	需求:利用一个请求方法,实现页面通用跳转
	 * 	页面url地址:
	 *  	/page/item-add
	 *   	/page/item-list
	 *   	/page/....
	 *
	 * 用法二:
	 * 	说明:利用restFul风格,可以简化用户url的写法
	 * 	例子:
	 * 		要求:利用同一个请求http://localhost:port/item  实现CRUD操作
	 * 			如:	http://localhost:port/item/save?xxxx
	 * 				http://localhost:port/item/delete?xxxx
	 * 				http://localhost:port/item/update?xxxx
	 * 		优化方案:http://localhost:port/item?xxxx
	 * 			type="GET"  	查询操作
	 * 			type="POST"		新增操作
	 * 			type="PUT"  	更新操作
	 * 			type="DELETE"  	删除操作
	 *
	 */
	@RequestMapping("/page/{moduleName}")
	public String module(@PathVariable String moduleName) {
		return moduleName;
	}
}
