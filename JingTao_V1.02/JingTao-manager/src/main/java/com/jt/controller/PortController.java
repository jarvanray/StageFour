package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jarvan
 * 2020/8/7 16:09
 */
@RestController
public class PortController {
    @Value("${server.port}")
    private Integer port;

    @RequestMapping("/getPort")
    public String getPort(){
        return "当前访问的端口:"+port;
    }
}
