package com.jt.service;

import com.jt.vo.ImageVo;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Jarvan
 * 2020/8/5 15:44
 */
public interface FileService {
    ImageVo upload(MultipartFile uploadFile);
}
