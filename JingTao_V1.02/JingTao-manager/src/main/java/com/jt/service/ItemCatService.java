package com.jt.service;

import com.jt.pojo.EasyUITree;
import com.jt.pojo.ItemCat;

import java.util.List;

/**
 * @author Jarvan
 * 2020/7/31 16:36
 */
public interface ItemCatService {

    ItemCat findItemCatById(Long itemCatId);

    List<EasyUITree> findItemCatListByParentId(Long parentId);

    List<EasyUITree> findItemCatCache(Long parentId);

}
