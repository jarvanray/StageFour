package com.jt.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.pojo.EasyUITable;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jt.mapper.ItemMapper;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {
	
	@Autowired(required = false)
	private ItemMapper itemMapper;

	@Autowired
	private ItemDescMapper itemDescMapper;

	//执行步骤:1.手动编辑sql  2.利用MP机制 动态生成
	/**
	 * 利用MP机制-动态生成
	 * @param page
	 * @param rows
	 * @return
	 */
	@Override
	public EasyUITable findItemAsPage(Integer page, Integer rows) {
		QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByDesc("updated");
		IPage<Item> iPage = new Page<>(page, rows);
		iPage = itemMapper.selectPage(iPage,queryWrapper);
		Long total = iPage.getTotal();
		List<Item> itemList = iPage.getRecords();
		return new EasyUITable(total,itemList);
	}

	/**
	 * 实现商品信息的入库操作
	 * 入库之前需要提前将数据补全,刚新增的商品应该处于正常状态(1)
	 * @param item
	 * warning:完成数据库的更新操作时,需要注意数据库的事务问题
	 */
	@Override
	@Transactional
	public void saveItem(Item item, ItemDesc itemDesc) {
		item.setStatus(1);
		itemMapper.insert(item);
				//.setCreated(new Date()).setUpdated(item.getCreated());
		itemDesc.setItemId(item.getId());
		itemDescMapper.insert(itemDesc);
	}

	@Override
	@Transactional
	public void updateItem(Item item,ItemDesc itemDesc) {
//		item.setUpdated(new Date());
		//根据对象中不为null的元素充当set条件,主键充当where条件
		itemMapper.updateById(item);
		itemDesc.setItemId(item.getId());
		itemDescMapper.updateById(itemDesc);
	}

	@Override
	@Transactional
	public void deleteItems(Long[] ids) {
		//method1:将数组转化为list集合
//		itemMapper.deleteBatchIds(Arrays.asList(ids));

		//method2:利用手写sql完成
		itemMapper.deleteItems(ids);
	}

	@Override
	@Transactional
	public void updateStatus(Long[] ids, Integer status) {
		//method1:MP
//		Item item = new Item();
//		item.setStatus(status);
//		//定义修改操作的条件构造器 where id in ()
//		UpdateWrapper<Item> updateWrapper = new UpdateWrapper<>();
//		List<Long> idList = Arrays.asList(ids);
//		updateWrapper.in("id",idList);
//		//根据MP机制实现批量数据的更新操作
//		itemMapper.update(item,updateWrapper);

		//method2:SQL
		itemMapper.updateStatus(ids,status);
	}

	@Override
	@Transactional
	public ItemDesc findItemDescById(Long itemId) {
		return itemDescMapper.selectById(itemId);
	}

	/**
	 * 分页sql
	 * SELECT * FROM TABLE LIMIT (N-1)ROWS,ROWS;
	 */
	/*@Override
	public EasyUITable findItemAsPage(Integer page, Integer rows) {
		long total = itemMapper.selectCount(null);
		int startIndex = (page-1)*rows;
		List<Item> itemList = itemMapper.findItemAsPage(startIndex,rows);
		return new EasyUITable(total,itemList);
	}*/
}
