package com.jt.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.anno.CacheFind;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.EasyUITree;
import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jarvan
 * 2020/7/31 16:38
 */
@Service
public class ItemCatServiceImpl implements ItemCatService {
    @Autowired(required = false)
    private ItemCatMapper itemCatMapper;
    @Autowired(required = false)  //(required = false)告诉容器该注入不是必须の
    private Jedis jedis;

    @Override
    public ItemCat findItemCatById(Long itemCatId) {
        return itemCatMapper.selectById(itemCatId);
    }

    /**
     * 1根据接口添加实现类的方法
     * 业务思路:
     *  1.用户传递的数据 @param parentId
     *  2.可以查询ItemCat数据库对象信息.
     *  3.动态的将ItemCat对象转化为EasyUITree对象
     *  4.
     * @return
     */
    @Override
    @CacheFind(key = "ITEM_CAT_LIST")       //使用注解,自定义前缀
    public List<EasyUITree> findItemCatListByParentId(Long parentId) {
        QueryWrapper<ItemCat> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",parentId);
        List<ItemCat> itemCatList = itemCatMapper.selectList(queryWrapper);
        List<EasyUITree> treeList = new ArrayList<>();  //先准备一个空集合.
        //需要将数据一个一个的格式转化.
        for(ItemCat itemcat :itemCatList){
            Long id = itemcat.getId();	//获取ID
            String text = itemcat.getName();	//获取文本
            //如果是父级,则默认应该处于关闭状态 closed, 如果不是父级 则应该处于打开状态. open
            String state = itemcat.isParent()?"closed":"open";
            //利用构造方法 为VO对象赋值  至此已经实现了数据的转化
            EasyUITree tree = new EasyUITree(id,text,state);
            treeList.add(tree);
        }
        //用户需要返回List<EasyUITree>
        return treeList;
    }

    @Override
    public List<EasyUITree> findItemCatCache(Long parentId){
        //1.准备key
        String key = "ITEM_CAT_LIST::" + parentId;
        List<EasyUITree> treeList = new ArrayList<>();
        //2.判断redis中是否有数据
        if(jedis.exists(key)){
            //表示key已存在不是第一次查询.直接从redis中获取数据.返回数据
            String json = jedis.get(key);
            treeList = ObjectMapperUtil.toObject(json, treeList.getClass());
            Long endTime = System.currentTimeMillis();
        }else{
            //表示key不存在,执行数据库查询
            treeList = findItemCatListByParentId(parentId);
            Long endTime = System.currentTimeMillis();
            //2.将数据转化为json
            String  json = ObjectMapperUtil.toJSON(treeList);
            //3.将返回值结果,保存到redis中. 是否需要设定超时时间???  业务
            jedis.set(key, json);

        }
        return treeList;
    }


}
