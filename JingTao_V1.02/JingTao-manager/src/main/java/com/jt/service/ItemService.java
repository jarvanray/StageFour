package com.jt.service;

import com.jt.pojo.EasyUITable;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;

public interface ItemService {

    EasyUITable findItemAsPage(Integer page, Integer rows);

    void saveItem(Item item, ItemDesc itemDesc);

    void updateItem(Item item,ItemDesc itemDesc);

    void deleteItems(Long[] ids);

    void updateStatus(Long[] ids, Integer status);

    ItemDesc findItemDescById(Long itemId);
}
