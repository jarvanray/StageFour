package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author Jarvan
 * 2020/8/5 10:38
 */
@Data
@Accessors(chain = true)
@TableName("tb_item_desc")
public class ItemDesc extends BasePojo{
    @TableId  //只标识主键
    private Long itemId;  //要求与商品表ID保持一致
    private String itemDesc;
    private Date created;
    private Date updated;
}
