package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author Jarvan
 * 2020/8/22 15:27
 */
@TableName("tb_cart")
@Data
@Accessors(chain = true)
public class Cart extends BasePojo implements Serializable {
    private static final long serialVersionUID = 902728393807839536L;
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long userId;
    private Long itemId;
    private String itemTitle;
    private String itemImage;
    private Long itemPrice;
    private Integer num;
}
