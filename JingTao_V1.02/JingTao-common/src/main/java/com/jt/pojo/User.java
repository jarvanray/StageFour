package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;


/**
 * @author Jarvan
 * 2020/8/18 11:40
 */
@TableName("tb_user")
@Data
@Accessors(chain = true)
public class User extends BasePojo implements Serializable {
    private static final long serialVersionUID = -1109985948516779567L;
    @TableId(type = IdType.AUTO)
    private Long id;
    private String username;
    private String password;
    private String phone;
    private String email;

}
