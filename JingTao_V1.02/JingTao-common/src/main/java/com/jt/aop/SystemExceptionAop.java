package com.jt.aop;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.vo.SysResult;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Jarvan
 * 2020/8/4 11:57
 */
@RestControllerAdvice  //定义异常处理的通知,只拦截controller层抛出的异常,并且返回json串
public class SystemExceptionAop {
    /**
     * 如果跨域访问发生了异常,则返回值必须经过特殊的格式封装
     * 如果是跨域访问形式,全局异常处理,可以正常的返回结果
     * 思路:
     *     1.判断用户提交参数中是否有callback参数
     *     2.
     * @param e
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public Object fail(Exception e, HttpServletRequest request){

        //1.获取用户请求的参数
        String callback = request.getParameter("callback");
        //2.判断参数是否有值
        if (StringUtils.isEmpty(callback)){
            //为空则用户的请求不是jsonp的跨域访问形式
            //打印异常信息
            e.printStackTrace();
            return SysResult.fail();
        }else {
            //是jsonp的报错信息
            e.printStackTrace();
            return new JSONPObject(callback,SysResult.fail());
        }
    }

}
