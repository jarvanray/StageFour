package com.jt.service;

import com.jt.pojo.User;

/**
 * @author Jarvan
 * 2020/8/21 11:25
 */
public interface DubboUserService {

    void saveUser(User user);

    String doLogin(User user);
}
