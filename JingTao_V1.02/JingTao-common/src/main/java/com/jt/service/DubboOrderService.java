package com.jt.service;

import com.jt.pojo.Order;

/**
 * @author Jarvan
 * 2020/8/24 14:22
 */
public interface DubboOrderService {
    String saveOrder(Order order);

    Order findOrderById(String id);
}
