package com.jt.service;

import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;

/**
 * @author Jarvan
 * 2020/8/22 11:10
 */
public interface DubboItemService {

    Item findItemById(Long itemId);

    ItemDesc findItemDescById(Long itemId);
}
