package com.jt.service;

import com.jt.pojo.Cart;

import java.util.List;

/**
 * @author Jarvan
 * 2020/8/22 15:26
 */
public interface DubboCartService {


    void updateCartNum(Cart cart);

    List<Cart> findCartList(Long userId);

    void deleteCart(Cart cart);

    void saveCart(Cart cart);

    List<Cart> findCartListByUserId(Long userId);
}
