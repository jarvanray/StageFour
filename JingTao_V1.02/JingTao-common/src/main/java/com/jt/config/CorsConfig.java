package com.jt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Jarvan
 * 2020/8/18 10:49
 */
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    //扩展跨域请求的方法
    @Override
    public void addCorsMappings(CorsRegistry registry){
        //1.允许什么样的请求进行跨域
        registry.addMapping("/**")     // /*只允许一级目录请求  /**允许多级目录请求
        //2.允许哪些服务器进行跨域
                .allowedOrigins("*")
        //3.是否允许携带cookie信息
                .allowCredentials(true)
        //4.定义探针检测时间:在规定的时间内不再
                .maxAge(1800);
    }
}
