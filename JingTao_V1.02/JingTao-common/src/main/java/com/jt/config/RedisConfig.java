package com.jt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import redis.clients.jedis.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration  //标识我是配置类
@PropertySource("classpath:/properties/redis.properties")
public class RedisConfig {

    //redis 集群
    @Value("${redis.nodes}")
    private String nodes;

    @Bean
    public JedisCluster jedisCluster(){
        String[] strNodes = nodes.split(",");
        Set<HostAndPort> nodes = new HashSet<>();

        for (String node:strNodes){     //node=ip:port
            String host = node.split(":")[0];
            int port = Integer.parseInt(node.split(":")[1]);
            nodes.add(new HostAndPort(host,port));
        }
        return new JedisCluster(nodes);
    }

/*

    @Value("${redis.nodes}")
    private String nodes;

    */
/**
     * spring整合Redis的分片机制
     *
     *//*

    @Bean
    public ShardedJedis shardedJedis(){
        //1.获取每个节点的信息
        String[] strNodes = nodes.split(",");
        List<JedisShardInfo> shardInfos = new ArrayList<>();
        //2.遍历所有的node,位list集合赋值
        for (String node:strNodes){     //node=ip:port
            String host = node.split(":")[0];
            int port = Integer.parseInt(node.split(":")[1]);
            JedisShardInfo info = new JedisShardInfo(host,port);
            shardInfos.add(info);
        }
        return new ShardedJedis(shardInfos);
    }
*/

/*
    @Value("${redis.host}")
    private String  host;
    @Value("${redis.port}")
    private Integer port;

    @Bean
    public Jedis jedis(){
        //数据写死?????????
        return new Jedis(host,port);
    }
*/

}

