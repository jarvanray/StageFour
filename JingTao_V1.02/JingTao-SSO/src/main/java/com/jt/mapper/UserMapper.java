package com.jt.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import org.apache.ibatis.annotations.Select;

/**
 * @author Jarvan
 * 2020/8/18 11:50
 */

public interface UserMapper extends BaseMapper<User>{
    @Select("select * from tb_user where binary username=#{username} and password=#{password}")
    User doLogin(User user);
}
