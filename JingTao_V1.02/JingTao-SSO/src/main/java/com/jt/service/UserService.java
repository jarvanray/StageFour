package com.jt.service;

import com.jt.pojo.User;

/**
 * @author Jarvan
 * 2020/8/18 11:50
 */
public interface UserService {

    boolean checkUser(String param, Integer type);

    void saveHttpClient(User userPOJO);
}
