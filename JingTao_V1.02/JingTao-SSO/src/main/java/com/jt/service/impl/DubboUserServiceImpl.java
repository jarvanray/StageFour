package com.jt.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.service.DubboUserService;
import com.jt.util.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import redis.clients.jedis.JedisCluster;

import java.util.UUID;

/**
 * @author Jarvan
 * 2020/8/21 11:30
 */
@Service
public class DubboUserServiceImpl implements DubboUserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private JedisCluster jedisCluster;

    /**
     * 步骤:
     * 	1.校验用户信息是否正确
     *  2.如果正确则利用redis保存到   key/value
     *  3.返回用户秘钥
     */
    @Override
    public String doLogin(User user) {
        String password = DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        //查询数据库检查是否正确   根据对象中不为null的属性充当where条件
//        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("username",user.getUsername())
//                    .eq("password",password);
//        User userDB = userMapper.selectOne(queryWrapper);
        user.setPassword(password);
        User userDB = userMapper.doLogin(user);
        //校验数据是否有效
        if(userDB == null) {
            return null;
        }
        //用户名和密码正确.  开始单点登录
        String ticket = UUID.randomUUID().toString().replace("-", "");
        //防止涉密信息泄露,则需要进行脱敏处理
        userDB.setPassword("你猜猜,看看能不能猜对!!!!");
        String value = ObjectMapperUtil.toJSON(userDB);
        //保证原子性
        jedisCluster.setex(ticket, 7*24*3600, value);
        return ticket;
    }

    /**
     * 1.密码加密
     * 2.邮箱暂由手机代替
     * @param user
     */
    @Override
    @Transactional
    public void saveUser(User user) {
        String md5Password = DigestUtils.md5DigestAsHex(user.getPassword().getBytes());
        user.setPassword(md5Password).setEmail(user.getPhone());
        userMapper.insert(user);
    }


}
