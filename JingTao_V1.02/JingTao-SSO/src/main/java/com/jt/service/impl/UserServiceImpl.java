package com.jt.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jarvan
 * 2020/8/18 11:51
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    //采用工具API形式动态获取
    private static Map<Integer,String> typeMap = new HashMap<>();
    static {
        typeMap.put(1,"username");
        typeMap.put(2,"phone");
        typeMap.put(3,"email");
    }

    /**
     * 查询数据库检查是否有数据
     * @param param
     * @param type
     * @return
     */
    @Override
    public boolean checkUser(String param, Integer type) {
        //1.根据参数类型获取校验的类型column
       String column = typeMap.get(type);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(column,param);
        int count = userMapper.selectCount(queryWrapper);
        return count==0?false:true;
    }

    @Override
    public void saveHttpClient(User userPOJO) {
        userMapper.insert(userPOJO);
    }
}
