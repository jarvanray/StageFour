package com.jt.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.util.ObjectMapperUtil;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisCluster;

/**
 * @author Jarvan
 * 2020/8/18 11:52
 */
@RestController
@RequestMapping("/user/")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private JedisCluster jedisCluster;

    /**
     * 完成用户名称回显
     * 参数:ticket信息
     * 返回值:
     * @param
     * @return
     */
    @RequestMapping("query/{ticket}")
    public JSONPObject findUserByTicket(@PathVariable String ticket,String callback){
        String userJSON = jedisCluster.get(ticket);
        if (StringUtils.isEmpty(userJSON)){
            return new JSONPObject(callback,SysResult.fail());
        }else {
            return new JSONPObject(callback,SysResult.success(userJSON));
        }
    }

    @RequestMapping("/httpClient/saveUser")
    public SysResult saveUser(String user){
        //1.将userjson的数据转化为user对象
        User userPOJO = ObjectMapperUtil.toObject(user, User.class);
        userService.saveHttpClient(userPOJO);
        return SysResult.success();
    }

    /**
     * 1.url请求地址:sso.jt.com/user/check/{param}/{type}
     * 2.请求参数: {需要校验的数据}/{需要校验的类型}
     * 3.@return SysResult 包含true/false
     * 4.JSONP请求方式:返回值必须经过特殊格式封装->callback(json)
     */
    @RequestMapping("check/{param}/{type}")
    public JSONPObject checkUser(@PathVariable String param, @PathVariable Integer type,String callback){
        //1.校验数据库中是否存在该数据
        boolean flag = userService.checkUser(param,type);  //存在true,不存在false
        //int a = 1/0;
        return new JSONPObject(callback,SysResult.success(flag));
    }

    @RequestMapping("/getMsg")
    public String getMsg(){
        return "sso单点测试成功!";
    }



}
