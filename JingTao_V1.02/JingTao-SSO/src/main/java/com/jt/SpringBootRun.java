package com.jt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Jarvan
 * 2020/8/18 11:45
 */
@SpringBootApplication
@MapperScan("com.jt.mapper")       //指定mapper接口包路径
public class SpringBootRun {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootRun.class,args);
    }
}
