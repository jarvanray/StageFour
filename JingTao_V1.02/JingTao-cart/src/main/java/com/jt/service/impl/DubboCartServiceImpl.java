package com.jt.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.CartMapper;
import com.jt.pojo.Cart;
import com.jt.service.DubboCartService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * @author Jarvan
 * 2020/8/22 15:30
 */
@Service
public class DubboCartServiceImpl implements DubboCartService {

    @Autowired
    private CartMapper cartMapper;

    @Override
    public void updateCartNum(Cart cart) {
        //1.
        Cart cartTemp = new Cart();
        cartTemp.setNum(cart.getNum());
        UpdateWrapper<Cart> updateWrapper = new UpdateWrapper();
        //根据itemID和userID可以确定唯一购物行为
        updateWrapper.eq("user_id",cart.getUserId())
                     .eq("item_id",cart.getItemId());
        cartMapper.update(cartTemp,updateWrapper);
    }

    @Override
    public List<Cart> findCartList(Long userId) {
        QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        return cartMapper.selectList(queryWrapper);
    }

    @Override
    public void deleteCart(Cart cart) {
        //将对象中不为null的元素当作where条件
        cartMapper.delete(new QueryWrapper<>(cart));
    }

    @Override

    public void saveCart(Cart cart) {
        //1.先查询数据库中是否有该记录  itemId和userId
        QueryWrapper<Cart> queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", cart.getUserId())
                    .eq("item_id", cart.getItemId());
        Cart cartDB = cartMapper.selectOne(queryWrapper);

        if(cartDB == null) {
            //说明第一次加购物车,直接入库
            /*cart.setCreated(new Date())
                .setUpdated(cart.getCreated());*/
            cartMapper.insert(cart);
        }else {
            //只更新商品数量
            int num = cart.getNum() + cartDB.getNum();
            Cart cartTemp = new Cart();
            cartTemp.setId(cartDB.getId())
                    .setNum(num);
//                    .setUpdated(new Date());
            cartMapper.updateById(cartTemp); //根据主键更新数据库.

            //自己手动操作Sql
//            cartMapper.updateCartNum(cartDB.getId(),num,new Date());
        }
    }

    @Override
    public List<Cart> findCartListByUserId(Long userId) {
        QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        return cartMapper.selectList(queryWrapper);
    }
}
