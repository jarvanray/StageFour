package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Cart;
import org.apache.ibatis.annotations.Update;

import java.util.Date;

/**
 * @author Jarvan
 * 2020/8/22 15:29
 */
public interface CartMapper extends BaseMapper<Cart> {
    @Update("update tb_cart set num=#{num},updated =#{date} where id = #{id}")
    void updateCartNum(Long id, Integer num, Date date);
}
