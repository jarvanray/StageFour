package com.jt.service;

import com.jt.pojo.User;

/**
 * @author Jarvan
 * 2020/8/18 17:29
 */
public interface HttpClientService {
    void saveUser(User user);
}
