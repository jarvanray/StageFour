package com.jt.service.impl;

import com.jt.pojo.User;
import com.jt.service.HttpClientService;
import com.jt.util.ObjectMapperUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author Jarvan
 * 2020/8/18 17:30
 */
@Service
public class HttpClientServiceImpl implements HttpClientService {

    @Override
    public void saveUser(User user) {
        //1.将user对象转化为json
        String userJSON = ObjectMapperUtil.toJSON(user);
        String url = "http://sso.jt.com/user/httpClient/saveUser?user="+userJSON;
        HttpClient httpClient = HttpClients.createDefault();
        HttpGet get = new HttpGet(url);
        try {
            HttpResponse httpResponse = httpClient.execute(get);
            //获取返回值状态信息
            int status = httpResponse.getStatusLine().getStatusCode();
            if (status!=200){
                throw new RuntimeException("请求错误");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("请求错误");
        }
    }
}
