package com.jt.handler;

import com.jt.pojo.User;
import com.jt.util.CookieUtil;
import com.jt.util.ObjectMapperUtil;
import com.jt.util.UserThreadLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import redis.clients.jedis.JedisCluster;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Jarvan
 * 2020/8/24 9:19
 */

@Component
public class UserInterceptor implements HandlerInterceptor {

    private static final String TICKET="JT_TICKET";
    private static final String JTUSER="JT_USER";

    @Autowired
    private JedisCluster jedisCluster;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Cookie cookie = CookieUtil.getCookieByName(TICKET, request);
        if (cookie != null){        //不为空,表示用户可能已经登录
            String ticket = cookie.getValue();
            if (jedisCluster.exists(ticket)){
                //获取真实的用户信息
                String userJSON = jedisCluster.get(ticket);
                User user = ObjectMapperUtil.toObject(userJSON, User.class);
                request.setAttribute(JTUSER,user);
                //利用ThreadLocal方式存储数据
                UserThreadLocal.set(user);
                return true;
            }else {
                //cookie中的记录与redis中的记录不一致,应该删除cookie中的数据
                CookieUtil.deleteCookie(ticket,"/","jt.com",response);
            }
        }
        response.sendRedirect("/user/login.html");
        return false;
    }

    //数据移除
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        UserThreadLocal.remove();
    }
}
