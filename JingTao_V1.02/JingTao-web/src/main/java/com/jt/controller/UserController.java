package com.jt.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.User;
import com.jt.service.DubboUserService;
import com.jt.util.CookieUtil;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.JedisCluster;
import sun.security.pkcs11.wrapper.CK_AES_CTR_PARAMS;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Jarvan
 * 2020/8/17 15:57
 */
@Controller
@RequestMapping("/user/")
public class UserController {

    @Reference(check = false)       //消费者在启动时检查是否有服务的提供者
    private DubboUserService dubboUserService;

    @Autowired
    private JedisCluster jedisCluster;

    private static final String TICKET = "JT_TICKET";

    /**
     * 完成用户退出操作
     * 1.url: http://www.jt.com/user/logout.html
     * 2.没有传递参数
     * 3.返回值: string  重定向到系统首页
     * 业务实现思路:
     * 	 0.先获取cookie中的数据 NAME=JT_TICKET
     * 	 1.删除redis中的数据     key-value    key=cookie中的value
     * 	 2.删除cookie记录	   根据cookie名称  设置存活时间即可.
     *
     * 注意事项: request对象中只能传递cookie的name和value.不能传递其他数据参数.
     * 所以如果需要再次操作cookie则最好设定参数,否则可能导致操作失败
     */
    @RequestMapping("logout")
    public String  logout(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = CookieUtil.getCookieByName(TICKET,request);
        if (cookie!=null){
            String ticket = cookie.getValue();
            if (!StringUtils.isEmpty(ticket)){
                jedisCluster.del(ticket);
                //删除cookie
                CookieUtil.deleteCookie(TICKET,"/","jt.com",response);
            }
        }
        //重定向到系统首页
        return "redirect:/";
    }

    /**
     * 1.url地址:http://www.jt.com/user/doLogin?r=0.7090733653647971
     * 2.params:{username:_username,password:_password}
     * 3.@return SysResult
     *
     * 需求:
     *  1.将cookie名称为"JT_TICKET"数据输出到浏览器中,要求7天超市,并且实现"jt.com"数据共享
     *
     *
     *  Cookie特点:
     *      1.浏览器中只能查看当前网址的cookie
     *      2.doMain 表示cookie共享的策略
     *          doMain: www.jd.com  当前的Cookie数据只能在当前域名中使用
     *          doMain: .jd.com  当前cookie是共享的可以在域名为jd.com结尾的域名中使用
     */
    @RequestMapping("doLogin")
    @ResponseBody
    public SysResult doLogin(User user, HttpServletResponse httpServletResponse){
        //1.通过user传递用户名和密码,交给业务层service进行校验,获取ticket信息(校验之后的回执)
        String ticket = dubboUserService.doLogin(user);
        if(StringUtils.isEmpty(ticket)) {
            //证明用户名或密码错误.
            return SysResult.fail();
        }
        //2.准备Cookie实现数据存储.
        Cookie cookie = new Cookie("JT_TICKET",ticket);
        cookie.setDomain("jt.com");
        cookie.setPath("/");
        cookie.setMaxAge(7*24*60*60); //7天超时
        //将cookie保存到客户端中.
        httpServletResponse.addCookie(cookie);
        return SysResult.success();
    }

    /**
     * 业务说明:完成用户注册入库
     *  1.url地址: http://www.jt.com/user/doRegister
     *  2.params: {}
     *  3.@return SysResult对象
     */
    @RequestMapping("doRegister")
    @ResponseBody
    public SysResult doRegister(User user){
        dubboUserService.saveUser(user);
        return SysResult.success();
    }


    /**
     * 通用跳转
     */
    @RequestMapping("{moduleName}")
//    @ResponseBody
    public String moduleName(@PathVariable String moduleName) {
        return moduleName;
    }


}
