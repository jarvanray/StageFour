package com.jt.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.service.DubboItemService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Jarvan
 * 2020/8/22 11:23
 */
@Controller  //需要跳转页面
@RequestMapping("/items/")
public class ItemController {
    @Reference(check = false)
    private DubboItemService dubboItemService;
    /**
     * 需求:根据商品ID查询商品信息(items/itemDesc)
     * url地址:http://www.jt.com/items/5646565.html
     * 参数:商品id
     * 页面取值要求:${item.title},${itemDesc.itemDesc}
     * 返回值:页面逻辑名称 item
     */
    @RequestMapping("{itemId}")
    public String findItemById(@PathVariable Long itemId, Model model) {
        //1.远程访问获取商品信息
        Item item = dubboItemService.findItemById(itemId);
        //2.远程访问获取商品描述信息
        ItemDesc itemDesc = dubboItemService.findItemDescById(itemId);
        //3.将数据传到页面中
        model.addAttribute("item", item);
        model.addAttribute("itemDesc", itemDesc);

        return "item";  //item.jsp
    }
}
