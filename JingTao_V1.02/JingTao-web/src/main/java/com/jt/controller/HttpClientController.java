package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.HttpClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jarvan
 * 2020/8/18 17:22
 */
@RestController
public class HttpClientController {

    @Autowired
    private HttpClientService httpClientService;

    @RequestMapping("/user/httpClient/saveUser/{username}/{password}/{phone}/{email}")
    public String saveUser(User user){
        httpClientService.saveUser(user);
        return "httpClient测试成功!";
    }
}
