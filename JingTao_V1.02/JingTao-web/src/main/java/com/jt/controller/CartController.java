package com.jt.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jt.pojo.Cart;
import com.jt.pojo.User;
import com.jt.service.DubboCartService;
import com.jt.util.UserThreadLocal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Jarvan
 * 2020/8/22 15:31
 */
@Controller
@RequestMapping("/cart/")
public class CartController {
    private static final String JTUSER="JT_USER";

    @Reference(check = false)
    private DubboCartService cartService;

    /**
     * 业务需求:购物车新增商品
     * 1.url://www.jt.com/cart/add/{item.id}.html
     * 2.params:cart的form表单
     * 3.@return 重定向到购物车展示页面(show.html)
     * warning:如果用户重复添加商品则只修改数量
     */
    @RequestMapping("/add/{itemId}")
    public String saveCart(Cart cart,HttpServletRequest request) {
       /* User user = (User)request.getAttribute(JTUSER);
        //1.获取userId
        Long userId = user.getId();*/
        Long userId = UserThreadLocal.get().getId();
        cart.setUserId(userId);
        cartService.saveCart(cart);

        return "redirect:/cart/show.html"; //维护伪静态策略
    }

    /**
     * 业务需求:删除购物车
     * 1.url:http://www.jt.com/cart/delete/562379.html
     * 2.params:itemId/userId
     * 3.@return:重定向到购物车列表页面
     */
    @RequestMapping("delete/{itemId}")
    public String deleteCart(Cart cart,HttpServletRequest request){
        /*User user = (User)request.getAttribute(JTUSER);
        //1.获取userId
        Long userId = user.getId();*/
        Long userId = UserThreadLocal.get().getId();
        cart.setUserId(userId);
        //2.执行删除业务操作
        cartService.deleteCart(cart);
        return "redirect:/cart/show.html";
    }

    /**
     * 业务需求:修改购物车数量
     * 1.url:http://www.jt.com/cart/update/num/562379/13
     * 2.参数:itemId/num  562379/13
     * 3.返回值:void
     */
    @RequestMapping("update/num/{itemId}/{num}")
    @ResponseBody
    public void updateCart(Cart cart,HttpServletRequest request){
        /*User user = (User)request.getAttribute(JTUSER);
        //1.获取userId
        Long userId = user.getId();*/
        Long userId = UserThreadLocal.get().getId();
        cart.setUserId(userId);
        cartService.updateCartNum(cart);
    }



    /**
     * 业务思路: 当用户点击购物车按钮时,应该根据userId查询购物车信息,之后在列表页面中展现.
     * 页面数据展现:  利用${cartList}展现数据
     * @return
     */
    @RequestMapping("show")
    public String show(Model model,HttpServletRequest request) {
       /* User user = (User)request.getAttribute(JTUSER);
        //1.获取userId
        Long userId = user.getId();*/
        Long userId = UserThreadLocal.get().getId();
        List<Cart> cartList = cartService.findCartList(userId);
        model.addAttribute("cartList",cartList);
        return "cart";
    }

}
