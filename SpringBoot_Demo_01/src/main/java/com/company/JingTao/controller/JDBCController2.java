package com.company.JingTao.controller;

import com.company.JingTao.pojo.User;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jarvan
 * 2020/7/29 9:12
 */
@RestController  //保证返回的数据转化为JSON
@ConfigurationProperties(prefix = "jdbc")  //定义属性的前缀
public class JDBCController2 {
    /* 1. 利用.yml配置文件的信息为属性赋值 */
    //批量为属性赋值时,要求配置文件的属性与类中的属性名称必须一致  ..自动赋值..
    private String username;  //定义数据库用户名
    private String password;  //定义数据库密码

    //为属性赋值时,一定会调用对象的set方法.
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    @RequestMapping("/getMsgPrefix")
    public String getMsgValue(){
        User user = new User();
        user
                .setId(10)
                .setName("gongqin")
                .setGender("male");
        return username + "\t" + password;
    }
}
