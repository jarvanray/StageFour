package com.company.JingTao.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author Jarvan
 * 2020/7/29 10:59
 */
@Data  //动态生成get/set/toString
@NoArgsConstructor  //动态生成无参构造
@AllArgsConstructor  //动态生成全参构造
@Accessors(chain = true)  //生成链式加载结构
public class User {
    private Integer id;
    private String name;
    private String gender;
}
