package com.jt.demo.controller;

import com.jt.demo.pojo.User;
import com.jt.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author Jarvan
 * 2020/7/30 9:45
 */
@Controller
@RequestMapping("/")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 需求:用户通过http://localhost:8080/findAll请求
     * 要求:1)跳转到userList.jsp页面中
     *      2)并且在页面中展现user列表数据
     */
    @RequestMapping("findAll")
    public String findAll(Model model){
        List<User> userList = userService.findAll();
        model.addAttribute("userList",userList);
        return "userList";
    }

    /**
     * 需求:用户通过http://localhost:8080/userAjax请求
     * 要求:1)跳转到userAjax.jsp页面中
     *      2)并且在页面中展现user列表数据
     */
    @RequestMapping("userAjax")
    public String userAjax(){
        return "userAjax";
    }

    /**
     * 接收ajax用户请求,返回值为userJson数据
     */
    @RequestMapping("findAjax")
    @ResponseBody
    public List<User> findAjax(){
        return userService.findAll();
    }
}
