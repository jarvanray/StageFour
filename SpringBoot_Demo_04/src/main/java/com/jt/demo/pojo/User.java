package com.jt.demo.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("user")  //将对象与表名关联
public class User {
	@TableId(type = IdType.AUTO)  //定义主键信息:主键自增
	private Integer id;
//	@TableField(value = "name")  //表示字段与属性的关联关系
	private String name;
	private Integer age;
	private String sex;

}
