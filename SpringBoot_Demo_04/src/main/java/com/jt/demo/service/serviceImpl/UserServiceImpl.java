package com.jt.demo.service.serviceImpl;

import com.jt.demo.mapper.UserMapper;
import com.jt.demo.pojo.User;
import com.jt.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jarvan
 * 2020/7/30 10:08
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.selectList(null);
    }
}
