package com.jt.demo.service;

import com.jt.demo.pojo.User;

import java.util.List;

/**
 * @author Jarvan
 * 2020/7/30 10:07
 */
public interface UserService {
    List<User> findAll();
}
