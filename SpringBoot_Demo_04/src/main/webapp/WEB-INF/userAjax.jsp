<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>您好Springboot</title>
	<script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
	<%-- js测试 --%>
	<script type="text/javascript">
		//让页面加载完成之后执行
		$(function () {
			//1. $.get  2. $.post  3. $.getJSON(只能获取json串)  4. $.ajax(万能用法)
			//1.语法:jQuery.get(url,[data],[callback],[type]) -->  url地址,data参数,回调函数,返回值类型(默认json串)
			$.get("findAjaxGet",function (data) {
				//1.获取返回值信息,之后遍历,将每个数据获取之后,拼接到table
				//ajax: 第一个参数代表下标,第二个参数代表遍历的对象
				var trs = null;
				$(data).each(function (index,user) {
					// var user = data[index];
					var id = user.id;
					var name = user.name;
					var age = user.age;
					var sex = user.sex;
					trs += "<tr align='center'><td>"+id+"</td><td>"+name+"</td><td>"+age+"</td><td>"+sex+"</td></tr>";
				});
				$("#tb1").append(trs);
			});

			//2.利用$.ajax方法发起ajax请求
			$.ajax({
				type: "post",  //请求类型
				url: "findAjax",  //请求路径
				dataType: "json",  //指定返回值格式为json串
				async: true,  //表示同步和异步问题,默认异步
				cache: false,  //添加请求缓存
				success: function (data) {
					$(data).each((index,user) => {
						addrows(user);
					})
				},
				error: function (data) {
					alert("请求失败!");
				}
			});
			function addrows(user) {
				var tr = "<tr align='center'><td>"+user.id+"</td><td>"+user.name+"</td><td>"+user.age+"</td><td>"+user.sex+"</td></tr>";
				$("#tb1").append(tr);
			}
		});
	</script>
</head>
<body>
	<table id="tb1" border="1px" width="65%" align="center">
		<tr>
			<td colspan="6" align="center"><h3>学生信息</h3></td>
		</tr>
		<tr>
			<th>编号</th>
			<th>姓名</th>
			<th>年龄</th>
			<th>性别</th>
		</tr>
		
		<c:forEach items="${userList}" var="u">
			<tr>
				<th>${u.id}</th>
				<th>${u.name}</th>
				<th>${u.age}</th>
				<th>${u.sex}</th>
			</tr>
		</c:forEach>
	</table>
</body>
</html>