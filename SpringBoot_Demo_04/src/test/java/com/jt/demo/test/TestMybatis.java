package com.jt.demo.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jt.demo.mapper.UserMapper;
import com.jt.demo.pojo.User;

@RunWith(SpringRunner.class)	//注意测试文件的位置 必须在主文件加载包路径下
@SpringBootTest
public class TestMybatis {
	
	@Autowired(required = false)
	private UserMapper userMapper;
	
	@Test
	public void testFindUser() {
		List<User> userList = userMapper.findAll();
		System.out.println(userList);
	}

	//
	@Test
	public void testSelectAll(){
		List<User> userList = userMapper.selectList(null);
		System.out.println(userList);
	}

	//1.用户入库操作
	//warning:MP操作时,将对象中不为null的数据 当做执行要素
	@Test
	public void insert(){
		User user = new User();
		user.setName("川普").setAge(60).setSex("男");
		int rows = userMapper.insert(user);
		System.out.println("执行成功...影响行数:"+rows);
	}

	//2.查询name="川普"的用户
	@Test
	public void selectUser(){
		//定义条件构造器  动态拼接where条件之后的语句
		User user = new User();
		user.setName("川普");
		QueryWrapper<User> queryWrapper = new QueryWrapper<>(user);
		List<User> userList = userMapper.selectList(queryWrapper);
		System.out.println(userList);
	}

	//3.查询sex="女" age>200的用户
	@Test
	public void selectUsers(){
		//逻辑运算符 =:eq , >:gt , <:lt , >=:ge , <=:le
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("sex","女");
		queryWrapper.le("age",20);
		List<User> userList = userMapper.selectList(queryWrapper);
		System.out.println(userList);
	}

	//4.查询name中含有"精"字的用户
	//4.1 查询name中 以"精"字结尾的用户 like %精
	@Test
	public void selectUserLike(){
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		//4.1
		queryWrapper.likeLeft("name","精");
		List<User> userList = userMapper.selectList(queryWrapper);
		System.out.println(userList);
	}

	//5. 查询age位于18-30之间,并且要求sex="女"
	//多条件查询,默认采用and关键字
	@Test
	public void selectUserAnd(){
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.between("age",18,30).eq("sex","女");
		List<User> userList = userMapper.selectList(queryWrapper);
		System.out.println(userList);
	}

	//6. 查询name不为null的用户信息,并且根据age降序排列,如果age相同则按照sex排序
	@Test
	public void selectUserDesc(){
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.isNotNull("name").orderByDesc("age","sex");
		List<User> userList = userMapper.selectList(queryWrapper);
		System.out.println(userList);
	}

	//7.批量查询数据  id=1,3,5,8,10,15,20
	@Test
	public void selectUserIn(){
		//1.idList自己进行封装
		//2.Id信息一般都是由前端进行传递,所以一般都是数组格式
		//warning: 一般在定义数组格式时,最好采用对象类型
		Integer[] idArray = {1,3,5,8,10,15,20};
		//需要将数组类型转化为集合
		List<Integer> idList = Arrays.asList(idArray);
		List<User> userList = userMapper.selectBatchIds(idList);
		System.out.println(userList);
	}

	//8.查询记录总数 name不为null
	@Test
	public void selectUserCount(){
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.isNotNull("name");
		int count = userMapper.selectCount(queryWrapper);
		System.out.println(count);
	}

	//9.删除用户信息
	@Test
	public void testDeleteUser(){
		//userMapper.deleteById(67);  删除主键信息
		//删除为null的数据
		//先查询再删除
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("name","测试案例");	//删除指定数据
		int delete = userMapper.delete(queryWrapper);
		System.out.println("删除成功!删除:"+delete+"条记录");
	}

	//10.修改用户信息 将name=null的用户信息name改为"测试案例" sex="男" age=1;
	@Test
	public void testUpdateUser() {
		//entity用户set数据.updateWrapper用户设定where条件
		User user = new User();
		user.setName("测试案例").setAge(1).setSex("男");
		UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
		updateWrapper.isNull("name");
		userMapper.update(user,updateWrapper);
		System.out.println("修改数据完成");
	}
}
